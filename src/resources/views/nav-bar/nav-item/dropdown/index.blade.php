<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="mediasiteDropdownMenuLink" data-toggle="dropdown"
       aria-haspopup="true" aria-expanded="false">
        Mediasite
    </a>
    <div class="dropdown-menu" aria-labelledby="mediasiteDropdownMenuLink">
        <a class="dropdown-item" href="{{route('mediasite.user-profiles.index')}}">User Profiles</a>
    </div>
</li>
