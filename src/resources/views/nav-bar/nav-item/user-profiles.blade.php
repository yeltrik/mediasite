@if (Route::has('mediasite.user-profiles.index'))
    <li class="nav-item">
        <a class="nav-link" href="{{ route('mediasite.user-profiles.index') }}">
            User Profiles
        </a>
    </li>
@endif
