@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action active">
                Mediasite
            </a>
            <a href="{{route('mediasite.user-profiles.index')}}" class="list-group-item list-group-item-action">
                User Profiles (Slow page to Load records from Mediasite)
            </a>
        </div>
    </div>
@endsection
