@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Mediasite User Profiles</h3>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">UserName</th>
                <th scope="col">Display Name</th>
                <th scope="col">Email</th>
                <th scope="col">Activated</th>
                <th scope="col">Presenter First</th>
                <th scope="col">Presenter Last</th>
                <th scope="col">Presenter Email</th>
            </tr>
            </thead>
            <tbody>

            @foreach($userProfiles as $userProfile)
                <tr>
                    <th scope="row">{{$userProfile['UserName']}}</th>
                    <th>{{$userProfile['DisplayName']}}</th>
                    <th>{{$userProfile['Email']}}</th>
                    <th>{{$userProfile['Activated']}}</th>
                    <th>{{$userProfile['PresenterFirstName']}}</th>
                    <th>{{$userProfile['PresenterLastName']}}</th>
                    <th>{{$userProfile['PresenterEmail']}}</th>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

{{--"odata.id" => "http://mediasite.wku.edu/Mediasite/Api/v1/UserProfiles('0016ec6c18604033aa8e33819225a73c38')"--}}
{{--"QuotaPolicy@odata.navigationLinkUrl" => "http://mediasite.wku.edu/Mediasite/Api/v1/UserProfiles('0016ec6c18604033aa8e33819225a73c38')/QuotaPolicy"--}}
{{--"#SetQuotaPolicyFromLevel" => array:1 [▶]--}}
{{--"Id" => "0016ec6c18604033aa8e33819225a73c38"--}}
{{--"UserName" => "jsc15968"--}}
{{--"DisplayName" => "Josie Coyle"--}}
{{--"Email" => "josie.coyle892@topper.wku.edu"--}}
{{--"Activated" => true--}}
{{--"HomeFolderId" => "dec373ffd3fc4238b9bab1809f229ae914"--}}
{{--"ModeratorEmail" => "josie.coyle892@topper.wku.edu"--}}
{{--"ModeratorEmailOptOut" => false--}}
{{--"DisablePresentationContentCompleteEmails" => true--}}
{{--"DisablePresentationContentFailedEmails" => false--}}
{{--"DisablePresentationChangeOwnerEmails" => true--}}
{{--"DisableCopyToFolderInitiatedEmails" => false--}}
{{--"DisableCopyToTrustedServerCompleteEmails" => false--}}
{{--"DisableCopyToTrustedServerFailEmails" => false--}}
{{--"DisableCopyToTrustedServerExportProjectFailEmails" => false--}}
{{--"DisableMoveToTrustedServerCompleteEmails" => false--}}
{{--"DisableMoveToTrustedServerFailEmails" => false--}}
{{--"TimeZone" => 19--}}
{{--"PresenterFirstName" => "Josie"--}}
{{--"PresenterMiddleName" => null--}}
{{--"PresenterLastName" => "Coyle"--}}
{{--"PresenterEmail" => "josie.coyle892@topper.wku.edu"--}}
{{--"PresenterPrefix" => null--}}
{{--"PresenterSuffix" => null--}}
{{--"PresenterAdditionalInfo" => null--}}
{{--"PresenterBio" => null--}}
{{--"TrustDirectoryEntry" => null--}}
{{--"ProviderStatus" => "Active"--}}
{{--"ProviderStatusDate" => "2020-04-08T14:01:13.177Z"--}}
