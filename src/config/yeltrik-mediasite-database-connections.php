<?php

use Illuminate\Support\Str;

return

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    [
        // Connection
        'mediasite' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('MEDIASITE_DB_DATABASE', database_path('mediasite.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        // SQLITE
        'mediasite_sqlite_example' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('MEDIASITE_DB_DATABASE', database_path('mediasite.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        // MYSQL
        'mediasite_mysql_example' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('MEDIASITE_DB_HOST', '127.0.0.1'),
            'port' => env('MEDIASITE_DB_PORT', '3306'),
            'database' => env('MEDIASITE_DB_DATABASE', 'mediasite'),
            'username' => env('MEDIASITE_DB_USERNAME', 'mediasite'),
            'password' => env('MEDIASITE_DB_PASSWORD', ''),
            'unix_socket' => env('MEDIASITE_DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

    ];
