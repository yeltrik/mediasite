<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\Mediasite\app\http\controllers\ReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('mediasite/report',
    [ReportController::class, 'index'])
    ->name('mediasite.reports.index');
