<?php

namespace Yeltrik\Mediasite\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    /**
     * UserProfileController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('mediasite::mediasite/report/index');
    }

}
