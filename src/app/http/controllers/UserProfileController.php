<?php

namespace Yeltrik\Mediasite\app\http\controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

ini_set('max_execution_time', 300);

class UserProfileController extends Controller
{

    /**
     * UserProfileController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     * @throws GuzzleException
     */
    public function index()
    {
        $client = new Client();

        $response = $client->request('GET', 'http://' . env('MEDIASITE_HOST') . '/Mediasite/api/v1/UserProfiles', [
            'headers' => [
                'sfapikey' => env('MEDIASITE_SFAPI_KEY'),
                'Host' => env('MEDIASITE_HOST'),
                'Authorization' => 'Basic ' . env('MEDIASITE_API_BASIC_AUTH'),
            ],
            'timeout' => 300
        ]);

        $userProfiles = json_decode($response->getBody(), TRUE)['value'];

        return view('mediasite::mediasite/user-profile/index', compact('userProfiles'));
    }

}
